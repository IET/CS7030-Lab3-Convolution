import numpy as np

from scipy import misc
from scipy import ndimage
from scipy import signal

import matplotlib.pyplot as plt
import matplotlib.cm as cm
import matplotlib.image as mpimg

img = mpimg.imread('StripedCat.png')

def convolve_2D(image, convolution_array):
	convolved_image = signal.convolve2d(img, arr)
	
	plot = plt.figure()
	sub = plot.add_subplot(1,1,1)
	sub.imshow(np.abs(convolved_image), cmap=cm.Greys_r)
	
	plt.show()
	
def convolve_fourier(image, convolution_array):
	return
	
#TODO - More Convolution Arrays
arr = np.array([[-1, -1, -1],[0,0,0],[1,1,1]])

convolve_2D(img, arr)
