import os, sys
import tkinter
from tkinter import *
from tkinter import filedialog
from PIL import Image 				#pip3 install pillow
from scipy import signal
import timeit
import numpy as np
import matplotlib.image as mpimg
import matplotlib.pyplot as plt
import matplotlib.cm as cm

fig_headings = []
conv_data = []
fft_data = []
num_plots = 0

def compare_performance(images, con_mat, title):
	global fig_headings
	global conv_data
	global fft_data
	global num_plots

	conv_times = []
	fft_times = []

	for test_img in images:
		#TODO - find a nicer (less ridiculous) way of calling timeit
		conv_times.append( (timeit.timeit("signal.convolve2d (img, con_mat)", setup = "import matplotlib.image as mpimg\nimg = mpimg.imread(\"{0}\")\nfrom scipy import signal\nimport numpy as np\ncon_mat = np.array({1})".format(test_img, con_mat), number=10)) )
		fft_times.append( (timeit.timeit("signal.fftconvolve(img, con_mat)", setup = "import matplotlib.image as mpimg\nimg = mpimg.imread(\"{0}\")\nfrom scipy import signal\nimport numpy as np\ncon_mat = np.array({1})".format(test_img, con_mat), number=10)) )
	
	print(title)
	print("-----------------------------------------------------")

	for i,(j,k) in enumerate(zip(conv_times, fft_times)):
		print ( "{0}% Image Resolution".format((i+1)*10))
		print ( "Time For Convolution: {0}	Time For FFT: {1} | Faster={2}".format(round(j, 5), round(k, 5), "FFT" if (k < j) else "Convolution") )
	print ("\n\n")

	#Update global data
	fig_headings.append(title)
	conv_data.append(conv_times)
	fft_data.append(fft_times)
	num_plots += 1

filetypes_list = [('Portable Network Graphics', '*.png'), ('JPEG', '*.jpg'), ('Bitmap', '*.bmp'), ('All Files', '*.*')]

print ("Please Select a Test Image")

root = Tk() #Create Tkinter Object
infile = filedialog.askopenfilename(parent=root, title="Please Select Image For Convolution", filetypes=filetypes_list)
root.destroy() #Exit Tkinter

print ("Selected: {0}\n".format(infile))

test_images = []

#Create images at various smaller resolutions
try:
	im = Image.open(infile)
	X = im.size[0]
	Y = im.size[1]
	
	for i in range(0, 10):
		im = Image.open(infile)
		size = (int(X*((i+1)/10)), int(Y*((i+1)/10)))
		im.thumbnail(size, Image.ANTIALIAS)
		outfile = infile.split('.')[0] + "_" + str((i+1)*10) + "%.png"
		im.save(outfile)
		test_images.append(outfile)
	
except IOError:
	print ("Failed to create scaled down images of {0}".format(infile))

#3x3 Gaussian Kernel
con_mat = "[[1,2,1],[2,4,2],[1,2,1]]"
compare_performance(test_images, con_mat, "3x3 Gaussian Kernel")

#5x5 Gaussian Kernel
con_mat = "[[1, 2, 4, 2, 1],[2,4,8,4,2],[4,8,16,8,4],[2,4,8,4,2],[1, 2, 4, 2, 1]]"
compare_performance(test_images, con_mat, "5x5 Gaussian Kernel")


#7x7 Gaussian Kernel
con_mat = "[[1,2,4,8,4,2,1],[2,4,8,16,8,4,2],[4,8,16,32,16,8,4],[8,16,32,64,32,16,8],[4,8,16,32,16,8,4],[2,4,8,16,8,4,2],[1,2,4,8,4,2,1]]"
compare_performance(test_images, con_mat, "7x7 Gaussian Kernel")

img_resolutions = [10, 20, 30, 40, 50, 60, 70, 80, 90, 100]
#Do plotting
fig,subplots = plt.subplots(num_plots, figsize=(12, 10), sharex=True)

for i, subplot in enumerate(subplots):
	subplot.set_title(fig_headings[i])
	subplot.scatter(img_resolutions, conv_data[i], color='b', label="Convolution")
	subplot.scatter(img_resolutions, fft_data[i], color = 'r', label="FFT")
	subplot.set_ylabel('Avg. Execution Time')
	leg = subplot.legend(loc=4)

subplots[-1].set_xlabel('% of Full Image Resolution')
	
fig.subplots_adjust(hspace=0.2)
#leg = plt.legend(loc=4)
#leg.get_frame().set_alpha(0.2)
plt.savefig("comparison_charts_{0}.png".format(infile.split('/')[-1]), dpi=96)
plt.show()

#Clean up working directory
for image in test_images:
	os.remove(image)
