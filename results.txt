Please Select a Test Image
Selected: U:/CS7030-Maths1/Lab4/source/StripedCat.png

3x3 Gaussian Kernel
-----------------------------------------------------
10% Image Resolution
Time For Convolution: 0.00236	Time For FFT: 0.00358 | Faster=Convolution
20% Image Resolution
Time For Convolution: 0.00867	Time For FFT: 0.00933 | Faster=Convolution
30% Image Resolution
Time For Convolution: 0.01877	Time For FFT: 0.02807 | Faster=Convolution
40% Image Resolution
Time For Convolution: 0.03379	Time For FFT: 0.05767 | Faster=Convolution
50% Image Resolution
Time For Convolution: 0.05372	Time For FFT: 0.10111 | Faster=Convolution
60% Image Resolution
Time For Convolution: 0.07768	Time For FFT: 0.14579 | Faster=Convolution
70% Image Resolution
Time For Convolution: 0.10631	Time For FFT: 0.20554 | Faster=Convolution
80% Image Resolution
Time For Convolution: 0.13982	Time For FFT: 0.27598 | Faster=Convolution
90% Image Resolution
Time For Convolution: 0.17757	Time For FFT: 0.34864 | Faster=Convolution
100% Image Resolution
Time For Convolution: 0.22012	Time For FFT: 0.47358 | Faster=Convolution



5x5 Gaussian Kernel
-----------------------------------------------------
10% Image Resolution
Time For Convolution: 0.00556	Time For FFT: 0.00363 | Faster=FFT
20% Image Resolution
Time For Convolution: 0.02075	Time For FFT: 0.00973 | Faster=FFT
30% Image Resolution
Time For Convolution: 0.04546	Time For FFT: 0.02466 | Faster=FFT
40% Image Resolution
Time For Convolution: 0.0839	Time For FFT: 0.04828 | Faster=FFT
50% Image Resolution
Time For Convolution: 0.12309	Time For FFT: 0.10876 | Faster=FFT
60% Image Resolution
Time For Convolution: 0.18436	Time For FFT: 0.14312 | Faster=FFT
70% Image Resolution
Time For Convolution: 0.23076	Time For FFT: 0.21257 | Faster=FFT
80% Image Resolution
Time For Convolution: 0.31691	Time For FFT: 0.27918 | Faster=FFT
90% Image Resolution
Time For Convolution: 0.41174	Time For FFT: 0.35085 | Faster=FFT
100% Image Resolution
Time For Convolution: 0.4803	Time For FFT: 0.45054 | Faster=FFT



7x7 Gaussian Kernel
-----------------------------------------------------
10% Image Resolution
Time For Convolution: 0.00905	Time For FFT: 0.00392 | Faster=FFT
20% Image Resolution
Time For Convolution: 0.03399	Time For FFT: 0.00999 | Faster=FFT
30% Image Resolution
Time For Convolution: 0.07166	Time For FFT: 0.02353 | Faster=FFT
40% Image Resolution
Time For Convolution: 0.1268	Time For FFT: 0.04938 | Faster=FFT
50% Image Resolution
Time For Convolution: 0.19581	Time For FFT: 0.10422 | Faster=FFT
60% Image Resolution
Time For Convolution: 0.28753	Time For FFT: 0.14487 | Faster=FFT
70% Image Resolution
Time For Convolution: 0.39057	Time For FFT: 0.22253 | Faster=FFT
80% Image Resolution
Time For Convolution: 0.50176	Time For FFT: 0.27486 | Faster=FFT
90% Image Resolution
Time For Convolution: 0.63497	Time For FFT: 0.35401 | Faster=FFT
100% Image Resolution
Time For Convolution: 0.81975	Time For FFT: 0.48858 | Faster=FFT



